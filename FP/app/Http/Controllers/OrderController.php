<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        // cek quantity

        $product = Product::find($id);
        $date = Carbon::now();

        if ($request->quantity > $product->quantity) {
            return back();
        }

        // cek order

        $cek_order = Order::where('user_id', auth()->user()->id)->where('status', 0)->first();

        if (empty($cek_order)) {
            Order::create([
                'user_id' => auth()->user()->id,
                'date' => $date,
                'total_price' => 0
            ]);
        }

        $new_order = Order::where('user_id', auth()->user()->id)->where('status', 0)->first();

        OrderDetail::create([
            'product_id' => $product->id,
            'order_id' => $new_order->id,
            'quantity' => $request->quantity,
            'price_amount' => $product->price * $request->quantity
        ]);

        $order = Order::where('user_id', auth()->user()->id)->where('status', 0)->first();
        $order->total_price += $product->price * $request->quantity;
        $order->update();

        return redirect('product');
    }

    public function checkout()
    {
        $order = Order::where('user_id', auth()->user()->id)->where('status', 0)->first();
        $order_details = OrderDetail::where('order_id', $order->id)->get();

        return view('frontend.checkout', compact('order', 'order_details'));
    }

    public function konfirmasi(){
        return view('frontend.konfirmasi');
    }
}
