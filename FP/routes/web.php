<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\OrderController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\{Route, Auth};


// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'FrontendController@index')->name('index');
Route::get('category/{id}', 'FrontendController@category');
Route::get('product', 'FrontendController@product');
Route::get('view-product/{id}', 'FrontendController@view_product');

Route::post('order/{id}', 'OrderController@index');
Route::get('checkout', 'OrderController@checkout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('role:admin')->group(function () {
    Route::get('/admin', function () {
        return view('admin.index');
    });
    Route::get('category', 'Admin\CategoryController@index');
    Route::get('category-create', 'Admin\CategoryController@create');
});

Route::get('/konfirmasi', 'OrderController@konfirmasi');