@extends('layouts.master')

@section('content')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="text-jumbotron">
                <h1 class="display-3">Fluid jumbotron</h1>
                <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of it's
                    parent.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="card-group">
                @foreach ($categories as $category)
                    <div class="card">
                        <img class="card-img-top" src="{{ $category->image }}" alt="Card image cap">
                        <a href="{{ url('category/' . $category->id) }}">
                            <div class="cat-text">
                                <h2>{{ $category->name }}</h2>
                                <p>2021 Collection</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <section class="product">
        <div class="heading">
            <h2>Product Overview</h2>
        </div>
        <hr class="hr-heading">
        <div class="container">
            <div class="row">
                @foreach ($products as $product)
                    <div class="col-md-3">
                        <div class="card prod p-1">
                            <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
                            <p>{{ $product->name }}</p>
                            <p>Rp. {{ number_format($product->price, 2) }}</p>
                            <p>Product Category : {{ $product->category->name }}</p>
                            <p>{{ $product->small_description }}</p>
                            <p>Stock : {{ $product->quantity }}</p>
                            <div class="text-product">
                                <a href="{{ url('view-product/' . $product->id) }}"
                                    class="btn-prod-custom btn-prod">Detail</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
