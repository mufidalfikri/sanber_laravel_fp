@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row my-5">
            <div class="col-lg-4 left-side-product-box pb-3">
                <img src="{{ $product->image }}" class="border p-2" width="360px">
            </div>
            <div class="col-lg-8">
                <div class="right-side-pro-detail border p-3 m-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="m-0 p-0">{{ $product->name }}</h4>
                        </div>
                        <div class="col-lg-12">
                            <h3 class="my-2 text-secondary">Rp. {{ number_format($product->price) }}</h3>
                            <hr class="p-0 m-0">
                        </div>
                        <div class="col-lg-12 my-3">
                            <h5>Product Detail</h5>
                            <span>{{ $product->description }}</span>
                            <hr class="my-3">
                        </div>
                        <div class="col-lg-12">
                            <p class="tag-section">Stok : {{ $product->quantity }}</p>
                            <hr class="my-3">
                        </div>
                        <div class="col-lg-12">
                            <h6>Quantity :</h6>
                            <form action="{{ url('order/' . $product->id) }}" method="post">
                                @csrf
                                <input type="number" name="quantity" class="form-control w-50 my-3" value="1">
                                <button type="submit" class="mt-2 btn btn-dark"><i
                                        class="fas fa-shopping-cart"></i>&nbsp;&nbsp; Add To Cart</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
