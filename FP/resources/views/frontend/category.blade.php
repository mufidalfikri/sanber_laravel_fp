@extends('layouts.master')

@section('content')
    <div class="container">
        <h1 class="text-center text-uppercase">{{ $category->name }}</h1>
        <hr class="hr-heading">
    </div>
    <div class="container">
        <div class="row py-5">
            @foreach ($products as $product)
                <div class="col-md-3">
                    <div class="card prod p-1">
                        <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
                        <p>{{ $product->name }}</p>
                        <p>Rp. {{ number_format($product->price, 2) }}</p>
                        <p>{{ $product->small_description }}</p>
                        <p>Stock : {{ $product->quantity }}</p>
                        <div class="text-product">
                            <a href="{{ url('view-product/' . $product->id) }}" class="btn-prod-custom btn-prod">Detail</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
