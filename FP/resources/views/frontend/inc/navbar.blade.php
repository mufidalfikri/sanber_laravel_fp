 <div class="nav-top d-flex justify-content-between align-content-center">
     <ul class="nav">
         <li class="nav-item">
             <a class="nav-link" href="#"><img src="{{ asset('images/site/indo.png') }}"
                     width="25px">&nbsp;&nbsp;indonesia</a>
         </li>
         <li class="nav-item">
             <a class="nav-link" href="#">
                 <i class="fas fa-phone"></i>&nbsp;&nbsp;(055)546334992
             </a>
         </li>
     </ul>
     <ul class="nav">
         @guest
             <li class="nav-item">
                 <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
             </li>
             @if (Route::has('register'))
                 <li class="nav-item">
                     <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                 </li>
             @endif
         @else
             <li class="nav-item">
                 <a id="navbarDropdown" class="nav-link" href="#" role="button" aria-haspopup="true"
                     aria-expanded="false" v-pre>
                     Selamat datang , <b>{{ Auth::user()->name }}</b>
                 </a>
             </li>
             <li>
                 <a class="nav-link" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                                                                                                                                                          document.getElementById('logout-form').submit();">
                     {{ __('Logout') }}
                 </a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                     @csrf
                 </form>
             </li>
         @endguest
     </ul>

 </div>

 <nav class="navbar navbar-expand-lg navbar-light bg-faded">
     <div class="container">
         <a class="navbar-brand" href="/">Store</a>
         <ul class="nav navbar-nav ml-auto">
             <li class="nav-item active">
                 <a class="nav-link" href="/">Home <span class="sr-only"></a>
             </li>
             <li class="nav-item">
                 <a class="nav-link" href="{{ url('product') }}">Products</a>
             </li>
             <li class="nav-item">
                 {{-- @php
                     $order = \App\Order::where('user_id', auth()->user()->id)
                         ->where('status', 0)
                         ->first();
                     if (!empty($order)) {
                         $notif = App\OrderDetail::where('order_id', $order->id)->count();
                     } else {
                         $notif = 0;
                     }
                 @endphp --}}
                 <a class="nav-link" href="{{ url('checkout') }}"><i class="fas fa-shopping-cart"></i> <span
                         class="badge badge-dark" style="vertical-align: top">0</span></a>
             </li>
             <li class="nav-item">
                 <a class="nav-link" href="#">Checkout</a>
             </li>
         </ul>
     </div>
 </nav>
