<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h5>Categories</h5>
                <ul>
                    <li><a href="">Women</a></li>
                    <li><a href="">Men</a></li>
                    <li><a href="">Accesories</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Help</h5>
                <ul>
                    <li><a href="">FAQ's</a></li>
                    <li><a href="">Shipping</a></li>
                    <li><a href="">Tracking</a></li>
                    <li><a href="">Return</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Get in touch</h5>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vitae dicta ducimus dignissimos
                    suscipit aspernatur consectetur!</p>
                <div class="media">
                    <i class="fab fa-facebook"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-instagram"></i>
                </div>
            </div>
            <div class="col-md-3">
                <h5>Newsletter</h5>
                <form action="">
                    <input type="email" class="form-control" placeholder="your email...">
                    <button class="btn btn-primary btn-sm mt-2">Subscribed</button>
                </form>
            </div>
        </div>
    </div>
</footer>
